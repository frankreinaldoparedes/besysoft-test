// Angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { GameScreenRoutingModule } from './game-screen-routing.module';

// Components
import { GameScreenComponent } from './game-screen-component/game-screen.component';

// Shared Module
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    GameScreenComponent,
  ],
  imports: [
    CommonModule,
    GameScreenRoutingModule,
    SharedModule
  ]
})
export class GameScreenModule { }
