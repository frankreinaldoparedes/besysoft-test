// Angular imports
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

// Tab Title
import { Title } from '@angular/platform-browser';

// Enum
import { GameScreenEnum as Enum } from './enum/game-screen-enum';

// Toast Service
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'game-screen',
  templateUrl: './game-screen.component.html',
  styleUrls: ['./game-screen.component.css']
})
export class GameScreenComponent implements OnInit {

  // Minimum and Maximum limits
  private readonly min: number = Enum.minValue;
  private readonly max: number = Enum.maxValue;

  // random number that were calculated
  randomNumber: number = Enum.defaultValue;

  // Enum
  enum = Enum;

  // Reactive Form
  formGroup: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    private toast: ToastrService,
    private router: Router,
    private title: Title,
  ) {
    this.formGroup = this.formBuilder.group({});
  }

  ngOnInit (): void {
    // Set the title of the tab
    this.title.setTitle(this.enum.tabTitle);

    // Init the form group
    this.initForm();

    // Assign a ramdom number between the range
    this.randomNumber = this.generateRandomNumber(this.min, this.max);

    // Information only for developers
    console.info(`${this.enum.consoleMessage} ${this.randomNumber}`);

    // Listening changes in the input
    this.listenChanges();
  }

  // Listening changes in the input to check the value
  listenChanges (): void {
    this.formGroup.get(this.enum.controlName)?.valueChanges.subscribe( (change: number) => {
      if (change < this.min) {
        this.toast.error(this.enum.errorMessageLower);

        this.formGroup.reset();
      }
      else if (change > this.max) {
        this.toast.error(this.enum.errorMessageHigher);

        this.formGroup.reset();
      }
    });
  }

  // Init the form group
  initForm (): void {
    this.formGroup = this.formBuilder.group({
      number: [
        null, 
        Validators.required,
      ],
    });
  }

  // This function generates a ramdom number between two numbers
  generateRandomNumber (min: number, max: number): number {
    return Math.floor( ( Math.random() * ( max - min + 1 ) ) + min );
  }

  // Check if the last number is a winning number
  checkPlay (): void {
    let control: AbstractControl = this.formGroup.get(this.enum.controlName) as FormGroup;

    if ( control.valid ) {
      if ( this.isWinningMove(control.value) ) {
        this.showSuccess();

        this.router.navigate([this.enum.link]);
      }
      else if ( this.numberIsGreater(control.value) ) {
        this.showFail(this.enum.greaterNumberMessage);
      }
      else {
        this.showFail(this.enum.lowerNumberMessage);
      }

      control.reset();
    }
    else {
      this.toast.warning(this.enum.typeAnumberMessage);
    }
  }

  // Determines if the number is equal to the random number
  isWinningMove (value: number): boolean {
    if (value === this.randomNumber) {
      return true;
    }
    
    return false;
  }

  // Determines if the number is greater or lower than the random number
  numberIsGreater (value: number): boolean {
    if (value > this.randomNumber) {
      return true;
    }
    
    return false;
  }

  // Show success message
  showSuccess (): void {
    this.toast.success(this.enum.winNumberMessage);
  }

  // Show fail message
  showFail (message: string): void {
    this.toast.warning(message);
  }
}