export enum GameScreenEnum {
    // HTML
    title = 'Adivine el número aleatorio!',
    buttonTitle = 'Comprobar',
    tabTitle = 'Random number game',
    inputPlaceholder = 'Escriba un número',
    label = 'Ingrese un número entre 0 y 100',
    labelSecondLine = 'para Jugar',
    
    // TS
    defaultValue = 0,
    minValue = 0,
    maxValue = 100,
    consoleMessage = 'Random Number =',
    controlName = 'number',
    winNumberMessage = 'Usted ha adivinado el numero, ¡FELICITACIONES!',
    greaterNumberMessage = 'El numero es Mayor al número aleatorio',
    lowerNumberMessage = 'El numero es Menor al número aleatorio',
    typeAnumberMessage = 'Debe ingresar un número',
    errorMessageLower = 'El número debe ser mayor o igual a 0',
    errorMessageHigher = 'El número debe ser menor o igual a 100',
    link = '/welcome',
}