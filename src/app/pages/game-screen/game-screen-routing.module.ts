import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameScreenComponent } from './game-screen-component/game-screen.component';

const routes: Routes = [
  {
    path: '', 
    pathMatch : 'full', 
    component: GameScreenComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameScreenRoutingModule { }
