// Angular imports
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { WelcomeScreenRoutingModule } from './welcome-screen-routing.module';

// Components
import { WelcomeScreenComponent } from './welcome-screen-component/welcome-screen.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    WelcomeScreenComponent
  ],
  imports: [
    CommonModule,
    WelcomeScreenRoutingModule,
    SharedModule
  ]
})
export class WelcomeScreenModule { }
