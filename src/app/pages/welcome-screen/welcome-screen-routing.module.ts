// Angular imports
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { WelcomeScreenComponent } from './welcome-screen-component/welcome-screen.component';


// Routes
const routes: Routes = [
  {
    path: '',
    pathMatch : 'full', 
    component: WelcomeScreenComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeScreenRoutingModule { }
