export enum WelcomeScreenEnum {
    // HTML
    title = 'Bienvenido a Besysoft Games!',
    buttonTitle = 'JUGAR',
    tabTitle = 'Besysoft Games - Welcome',
    link = '/game',
}