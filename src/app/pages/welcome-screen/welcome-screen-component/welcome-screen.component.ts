// Angular imports
import { Component, OnInit } from '@angular/core';

// Tab Title
import { Title } from '@angular/platform-browser';

// Enum
import { WelcomeScreenEnum as Enum } from './enum/welcome-screen-enum';


@Component({
  selector: 'welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrls: ['./welcome-screen.component.css']
})
export class WelcomeScreenComponent implements OnInit {
  
  // Enum
  enum = Enum;

  constructor(
    private title: Title
  ) {}

  ngOnInit(): void {
    // Set the title of the tab
    this.title.setTitle(this.enum.tabTitle);
  }

}
