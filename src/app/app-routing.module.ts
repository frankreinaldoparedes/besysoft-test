// Angular imports
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Routes
const routes: Routes = [
  {
    path: 'welcome', 
    pathMatch : 'full', 
    loadChildren: () => import('./pages/welcome-screen/welcome-screen.module').then(m => m.WelcomeScreenModule)
  },
  {
    path: 'game', 
    pathMatch : 'full', 
    loadChildren: () => import('./pages/game-screen/game-screen.module').then(m => m.GameScreenModule)
  },
  {
    path: '**', 
    redirectTo: 'welcome'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
