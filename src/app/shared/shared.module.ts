// ANgular Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Reactive Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Shared Modules
import { HeaderModule } from './header/header.module';



@NgModule({
  declarations: [],
  imports: [
    // Angular Modules
    ReactiveFormsModule,
    FormsModule,
    CommonModule,

    // Shared Modules
    HeaderModule
  ],
  exports: [
    // Angular Modules
    ReactiveFormsModule,
    FormsModule,

    // Shared Modules
    HeaderModule
  ]
})
export class SharedModule { }
